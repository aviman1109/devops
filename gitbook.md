# GitBook

## GitBook時序圖

{%plantuml%}

@startuml

title "DevOps - Sequence Diagram"
actor Developer
boundary "GitLab server" as GitLab
control "GitLab-Ci Runner" as gitlabCi
entity "Gitlab Page" as page
Developer -> GitLab : git push
GitLab -> gitlabCi : push event
gitlabCi -> page : build gitbook

@enduml  

{%endplantuml%}

如上圖，開發人員執行`git push`上傳至gitlab server

gitlab-ci runner 接收到`push event` 便會開始建置gitbook

最後將gitbook轉換成靜態網站，發佈至gitlab page上

## gitlab-ci runner

與drone一樣是ci工具，必須藉由_gitlab-ci runner_才能將gitbook發佈至gitlab page

與drone相似的是，相依於根目錄中的.gitlab-ci.yml

將會在以下章節提供使用說明


