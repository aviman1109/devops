# GitBook Editor

在GitBook中，使用的是markdown語法，但是如果要透過文字編輯器撰寫markdown文件，將會是一件痛苦的事情。

## 安裝

在這邊推薦使用gitbook官方的[ GitBook Editor](https://legacy.gitbook.com/editor)

點選[連結](https://legacy.gitbook.com/editor)，並下載安裝

## 忽略登入

![](/assets/Screenshot 2018-12-04_21-53-59-233.png)

在開啟gitbook editor的頁面中忽略登入，

因為gitbook在將這部分的功能搞砸了...

如果盲目的登入，也不會成功（我們也用不到）

## 開啟

![](/assets/Screenshot 2018-12-04_21-56-17-173.png)

在左上角選擇`Open`或者`import`

選擇open將會以所選擇的目錄進行開啟



使用import則會將整個專案導入GitBook的資料夾中

並且顯示於GitBook中（如下圖）

![](/assets/gitbookimport.png)



但是要注意GitBook路徑預設如下

| Platform | Directory |
| :--- | :--- |
| Windows | `C:/Users/username/GitBook/Library` |
| OSX | `/Users/username/GitBook/Library` |
| Linux | `/home/username/GitBook/Library` |

當然也可以透過`Change Library Path...`修改路徑

