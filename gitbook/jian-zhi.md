# GitBook建置

## 目錄結構

.  
├── .git  
├── .gitlab-ci.yml  
├── README.md  
├── SUMMARY.md  
└── book.json  

## `.gitlab-ci.yml`

{%ace edit=true, lang='yaml', theme='tomorrow_night' %}

# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master
  tags:
    - docker 

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path

  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch

{%endace%}

## README.md

書籍的第一頁內容是從 README.md 這個檔案生成的，即使你沒有將 README.md 擺進製書指引檔（SUMMARY.md），它還是會被自動加入。

## SUMMARY.md
GitBook 使用一個 SUMMARY.md 檔案來定義書籍的目錄架構，也就是多層次章節的設定。SUMMARY.md 檔案同時也被用來製作書籍目錄（TOC - Tables Of Contents）。

SUMMARY.md 的格式只是簡單的連結列表，連結的「名稱」就是章節的「標題」，連結標的則是實際的內容「檔案」（包含路徑在內）。

章節的層級，就根據清單的層級進行定義。
### `SUMMARY.md`範例

{%ace edit=true, lang='markdown', theme='tomorrow_night' %}

# Summary

* [第一章](chapter1.md)
* [第二章](chapter2.md)
* [第三章](chapter3.md)

{%endace%}

## book.json

gitbook在編譯書籍的時候會讀取書籍源碼頂層目錄中的`book.js`或者`book.json`
可以透過這個文件設定整本書的參數

### `book.js`範例

{%ace edit=true, lang='json', theme='tomorrow_night' %}

{
  "title": "DevOps",
  "description": "自動化工具",
  "author": "Casper.Cheng",
  "gitbook": "3.2.3",
  "root": "."
  "links": {
    "sharing": {
        "all": null,
        "facebook": null,
        "google": null,
        "twitter": null,
        "weibo": null
    },
    "sidebar": {
        "Home": "https://mes_group.qbicloud.com/sipxpatchs/"
    }
  },
  "plugins": ["search-pro","puml",]

}

{%endace%}



