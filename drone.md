# Drone {#drone}

基於`Docker`的`CI/CD`工具`Drone`所有編譯、測試的流程都在`Docker`容器中進行。

開發者只需在項目中包含`.drone.yml`文件，將代碼推送到git倉庫，`Drone`就能夠自動化的進行編譯、測試、發布。

如下圖UML所示

{%plantuml%}

@startuml

title "DevOps - Sequence Diagram"

actor Developer

boundary "GitLab server" as GitLab

control "Drone Server" as Drone

entity Server

Developer -> GitLab : git push
GitLab -> Drone : push event
Drone -> Server : Deploy

@enduml  
{%endplantuml%}

以下示範`.drone.yml`文件




{%ace edit=true, lang='yaml', theme='tomorrow_night' %}

workspace:
  base: /drone
  path: world
  
kind: pipeline
name: default

clone:
  depth: 50
  tags: true

steps:
- name: build
  image: openjdk:11.0.1-oracle
  commands:
  - java -version
  - echo $JAVA_HOME
  - cd /drone/world/world-build/bin-sh/
  - ./build-linux.sh
  - ls -al /drone/world/world-build/build/
  
{%endace%}

這樣的內容在push至server後
可以完成在drone的container中包版


以下的章節中將會一一介紹drone的各個功能以及plugin



