# Git push

#### 使用`git push <remote name><branch name>`上傳分支

例如說我現在想把`developer`上傳到 Gitlab

```
$ git push gitlab developer
```

他會將本機端的`developer`分支，上傳到 server 上

如果 server 上沒有`developer`這個分支，他就會自動在 server 上添加`developer`分支

> 注意：  
> 本機端的分支名稱必須跟 server 上的分支名稱完全相同

![](/assets/push.png)我在上圖中，先在本地新增了名為`developer`的分支，再將這個分支進行`push`上傳至server

有關遠端的使用與管理，歡迎查看[相關文件](https://zlargon.gitbooks.io/git-tutorial/content/remote/)

