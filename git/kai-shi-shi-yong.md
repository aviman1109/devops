Git專案

開始使用

首先等入[公司內部gitlab](https://git.chainsea.com.tw/)

開啟目標專案

將會顯示以下內容

### Command line instructions

##### Git global setup

```
git config --global user.name "{{your name}}"
git config --global user.email "{{your email}}"
```

##### Create a new repository

```
git clone 
https://{{your name}}@git.chainsea.com.tw/{{project path}}/{{project name}}.git
cd {{project name}}
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

##### Existing folder

```
cd existing_folder
git init
git remote add origin 
https://{{your name}}@git.chainsea.com.tw/{{project path}}/{{project name}}.git
git add .
git commit
git push -u origin master
```

##### Existing Git repository

```
cd existing_repo
git remote add origin 
https://{{your name}}@git.chainsea.com.tw/{{project path}}/{{project name}}.git
git push -u origin --all
git push -u origin --tags
```



可以將專案內的指令逐一貼進command line中，記得選擇相對應的狀態。



