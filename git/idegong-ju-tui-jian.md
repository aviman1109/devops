# Git GUI Tool

很多人在接觸 Git 都會覺得它不容易學，除了不了解它運作的原理之外，另一個原因就是大部份的人都還是比較習慣圖形介面工具（GUI, Graphic User Interface），對終端機（或命令提示字元）以及 Git 指令操作相對的不熟悉。

所以在這邊提供幾個好用的ide，以利各位同仁在使用上方便。



## [SmartGit](https://www.syntevo.com/smartgit/)

![](https://www.syntevo.com/assets/images/products/smartgit/opener-481720cb.png "IMG")

這個Git GUI客戶端有一個簡單乾淨，整潔的UI，非常易於使用，並為您提供項目的精彩概述。  
它具有最合理的佈局和很好的過濾選項。

* **平台：**
  Windows，Linux，Mac
* **價格：**
  $ 209- $ 84 /用戶（終身）或$ 79- $ 47.40 /用戶（年度）或$ 5.99- $ 3.59 /用戶（每月訂閱）
* **Pull Request / Comments支持：**
  BitBucket，Atlassian Stash，GitHub，自己的Git服務器。
* **問題跟踪器支持：**
  Atlassian JIRA
* **免費：**
  許多人愛好使用，開源開發人員，公共教育機構（教師，學生）和公共慈善機構。

## [GitKraken](https://www.gitkraken.com/)

![](https://boostlog-public.s3.us-west-1.amazonaws.com/articles/5b3336b244deba0054047685/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-06-27%2015.56.46-1530082614860.png "IMG")

![](/assets/gitkraken.png)

這個跨平台的GUI客戶端絕對是一個非常適合初學者，因為它有很好的存儲庫管理工具。  
除此之外，它還支持開發人員為開發人員提供的不同pull機制。

* **平台：**
  Windows，Linux，Mac
* **價格：**
  Pro為49美元/用戶/年，Enterprise為~99美元/用戶/年。
* **集成：**
  GitHub，Bitbucket，GitLab
* **免費版本：**
  是（適用於教育，開源，非營利，初創公司或個人使用）

## [SourceTree](https://www.sourcetreeapp.com/)

![](https://boostlog-public.s3.us-west-1.amazonaws.com/articles/5b3336b244deba0054047685/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-06-27%2015.57.41-1530082675161.png "IMG")

其全面的佈局為您提供快速設置。  
它簡單而強大，允許在提交期間選擇塊和行。



* **平台：**
  Windows，Mac
* **價格：**
  免費

## [Tower 2](https://www.git-tower.com/mac)

![](https://boostlog-public.s3.us-west-1.amazonaws.com/articles/5b3336b244deba0054047685/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-06-27%2015.58.15-1530082712129.png "IMG")

它非常漂亮，提供了解決衝突的可視方式。  
這個外觀現代的UI為命令行git程序提供了堅實的前端。  
它具有使用Git所需的所有功能：分支，標記，合併以及輕鬆使用遠程存儲庫。



* **平台：**
  Windows;
  OSX

## [Fork](https://git-fork.com/)

![](https://git-fork.com/images/image1.jpg "IMG")

這款Mac和Windows Git客戶端非常友好且快速。  
在每個階段之後，無需將文件滾動視圖重置為文件頂部。  
可以輕鬆解決與合併衝突助手和內置合併衝突解析器的合併衝突。

* **平台：**
  MacOS

## [TortoiseGit](https://tortoisegit.org/)

![](https://boostlog-public.s3.us-west-1.amazonaws.com/articles/5b3336b244deba0054047685/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-06-27%2015.59.43-1530082791515.png "IMG")

現在訪問您的常用命令並不像以前那麼耗費時間，因為TortoiseGit為您提供了窗口上下文菜單集成，可以快速訪問常用命令。  
此外，它通過上下文菜單集成git-svn，並提供清晰易懂的提交日誌。



* **平台：**
  Windows

## [Gmaster](https://gmaster.io/)

![](https://boostlog-public.s3.us-west-1.amazonaws.com/articles/5b3336b244deba0054047685/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88%202018-06-27%2016.00.49-1530082865225.png "IMG")

通過令人驚嘆的 visual og gmaster清楚地了解正在發生的事情並使其有意義。  
它具有各種語言的合併工具，如Java，C＃，C，C ++，VB.Net和Delphi。

* **平台：**
  Windows
* **價格：**
  免費
* **許可證：**
  TBD
* **多語言支持：**
  沒有
* **免費：**
  個人和非營利組織
* **問題跟踪器支持：**
  是的

## [Git-Cola](https://git-cola.github.io/)

![](https://git-cola.github.io/images/screenshot-main-linux.png "IMG")

這個開源和高度可定制的GUI具有非常時尚和強大的圖形用戶界面。  
這個軟件是用python編寫的。

* **平台：**
  Windows，macOS，Linux
* **許可證：**
  GPLv2

## 



