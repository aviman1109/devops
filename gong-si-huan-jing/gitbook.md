# GitBook

GitBook 是一個基於Node.js的命令行工具，可使用Github / Git和Markdown來製作精美的電子書，GitBook並非關於Git的教程。

GitBook支持輸出多種文檔格式：

靜態網站：GitBook默認輸出該種格式，生成的靜態站點可直接託管搭載Github Pages服務上

* PDF：需要安裝gitbook-pdf工具
* eBook：需要安裝ebook-convert
* 單HTML網頁：支持將內容輸出為單頁的HTML，不過一般用在將電子書格式轉換為PDF或eBook的中間過程
* JSON：一般用於電子書的調試或元數據提取。



使用GitBook製作電子書，必備兩個文件：README.md和SUMMARY .md



並且可以使用許多plugin



以下為可供參考的網站

* [https://alinex.gitlab.io/env/gitbook.html](https://alinex.gitlab.io/env/gitbook.html)



