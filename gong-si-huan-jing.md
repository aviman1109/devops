# 公司環境

在DevOps中，公司內部的環境建置如下圖的UML所示

{%plantuml%}
@startuml
Title Drone and Git
hide empty description
State "Drone Server" as Drone
State "GitLab server" as GitLab
State "GitLab-Ci Runner" as gitlabCi
State Server
Drone : Build
Drone : Test web
note top of Drone
    port 443 by nginx
end note
GitLab : .git
note top of GitLab
    port 443 by nginx
end note
gitlabCi : ci runner
note top of Server
    port 22 for Drone
    //9122:22
end note
[*] -right-> GitLab : git push master
GitLab -right-> Drone : push event
Drone -right-> Server :Deploy by 22port
GitLab -down-> gitlabCi : push event
gitlabCi -up-> GitLab : build pages
Server -right-> [*] : ./server start
@enduml
{%endplantuml%}



將會在以下章節中發布公司內部目前使用的工具

