# 前置作業

以下以elipse 作為範例

新增一個名為`.drone.yml`的隱藏檔案

## 顯示隱藏檔案

首先點選`Project Explorer`右上角的倒三角形

![](/assets/be1.png)

點選`Filters and Customization...`

取消勾選 `.*resources`

點選 `OK`

這時候應該就可以看見隱藏的檔案

## 新增`.drone.yml`

在專案的根目錄下新增一個檔案

名為`.drone.yml`

如下圖

![](/assets/droneyml.png)

## 安裝yaml套件

在Eclipse Marketplace中搜尋yaml並點選安裝

![](/assets/eclipseyaml.png)





