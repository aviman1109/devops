# Drone Scp

`SCP`插件通過SSH將文件和工件複製到目標主機。

## 基本範例

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host: example.com
    target: /home/deploy/web
    source: release.tar.gz
{%endace%}

## 用戶名＆密碼＆port

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host: example.com
    username: appleboy
    password: 12345678
    port: 4430
    target: /home/deploy/web
    source: release.tar.gz
{%endace%}

## 多來源多目錄

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host: example.com
    target:
      - /home/deploy/web1
      - /home/deploy/web2
    source:
      - release_1.tar.gz
      - release_2.tar.gz
{%endace%}

## 多個遠端主機

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host:
      - example1.com
      - example2.com
    target: /home/deploy/web
    source: release.tar.gz
{%endace%}

## 目錄通用字符

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host:
      - example1.com
      - example2.com
    target: /home/deploy/web
    source:
      - release/*.tar.gz
{%endace%}

## 刪除功能

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host: example.com
    target: /home/deploy/web
    source: release.tar.gz
    rm: true
{%endace%}

## 密鑰密碼

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  - name: scp
    image: appleboy/drone-scp
    host:
      - example1.com
      - example2.com
    user: ubuntu
    port: 22
    secrets: [ ssh_password ]
    target: /home/deploy/web
    source:
      - release/*.tar.gz
{%endace%}

## 相關文獻

相關的文件可以至[官方文件](http://plugins.drone.io/appleboy/drone-scp/)查詢


