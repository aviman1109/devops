# Dron Cloning

在drone的自動化過程中，預設就會將整個專案clone下來，但是在這邊提供更多的技術細節，可以自定義`clone`的條件，並且可以使用額外的步驟進行擴展，使得整個自動化更貼近需求。



## `depth` {#depth}

默認`clone`配置未設置深度。您可以通過聲明`clone`塊並添加`depth`屬性來強制執行`clone`深度：

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}

kind: pipeline
name: default

clone:
  depth: 50

steps:
- name: build
  image: golang
  commands:
  - go build
  - go test
{%endace%}



## `disable`
在下文中，我們禁用默認`clone`行為，並將`clone`邏輯委託給pipeline中的自定義插件：

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}

kind: pipeline
name: default

clone:
  disable: true

steps:
- name: clone
  image: janecitizen/cloner

- name: build
  image: golang
  commands:
  - go build
  - go test
{%endace%}


## 相關文獻

相關的文件可以至[官方文件](https://docs.drone.io/config/pipeline/cloning/)查詢
