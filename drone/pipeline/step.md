# Drone steps

按照Drone官方的規範，需要定義`pipeline`

並且按造steps將個步驟分開

## QS7範例

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
#設定workspace
workspace:
  base: /drone
  path: world
  
#定義pipeline
kind: pipeline
name: default

#列出steps
steps:
- name: build #steps名稱
  image: openjdk:11.0.1-oracle #使用docker-hub上的版本
  commands:
  - java -version
  - echo $JAVA_HOME
  #建置QS
  - cd /drone/world/world-build/bin-sh/
  - ./build-linux.sh    
  - ls -al /drone/world/world-build/build/

{%endace%}

## 相關文獻

相關的文件可以至[官方文件](https://docs.drone.io/config/pipeline/steps/)查詢



