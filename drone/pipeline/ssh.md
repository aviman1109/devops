# Drone SSH

使用`SSH`插件在遠程服務器上執行命令。


## 基本範例

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
- name: ssh
  image: appleboy/drone-ssh
  host: foo.com
  username: root
  password: 1234
  port: 22
  script:
    - echo hello
    - echo world
{%endace%}

如上範例所示，在SSH中與SCP的使用方始相似，
只是將`target` `source`兩個部分置換成`script`


{%ace edit=true, lang='diff' , theme='tomorrow_night' %}
- - name: scp
+ - name: ssh
    image: appleboy/drone-scp
    host: example.com
    username: appleboy
    password: 12345678
    port: 4430
-   target: /home/deploy/web
-   source: release.tar.gz
+   script:
+     - echo hello
+     - echo world
{%endace%}