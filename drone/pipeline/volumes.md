# Drone Volumes

`Volumes`可用於訪問主機上的文件和文件夾，並在`pipeline steps`之間共享文件和文件夾。

## QS6範例
以下是QS6的自動化包版範例

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
  workspace:
    base: /drone
    path: pgw
  
  kind: pipeline
  name: default
  
  clone:
    depth: 50
  
  steps:
  - name: build
    image: java:openjdk-7-jdk
    group: build
    volumes:
    - name: ec2-user
      path: /home/ec2-user
    commands:
      - ls -al /home/ec2-user
      - java -version
      - echo $JAVA_HOME
      - cd /home/ec2-user/qs-build/output
      - mv *.zip ./old/ && echo "Success" || echo "Failure"
      - cd /home/ec2-user/qs-build/bin/local/304-bill
      - ./build-linux.sh
      - cd /home/ec2-user/qs-build/output
      - ls -al
  
   volumes:
   - name: ec2-user
     host:
       path: /home/ec2-user


{%endace%}

在以上範例中
可以將`/home/ec2-user`的資料夾共享進container中的`/home/ec2-user`資料夾

## 相關文獻

相關的文件可以至[官方文件](https://docs.drone.io/config/pipeline/volumes/)查詢


