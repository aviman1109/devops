# workspace

workspace定義所有pipeline steps共享的工作目錄。默認工作區根據您的存儲庫URL類似以下模式。

```
/drone/src/gitlab.com/{{your name}}/{{project name}}
```

## 自定義workspace

### `.drone.yml`

{%ace edit=true, lang='yaml' , theme='tomorrow_night' %}
workspace:
  base: /drone
  path: world
{%endace%}

如果這樣指定
則專案的目錄就會如以下所顯示

```
/drone/world

```

並且 `/drone/` 這個資料夾將共用於所有步驟中